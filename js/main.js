$(document).ready(function () {
    $(".c-btn__menu").click(function () {
        $(".c-menu").animate({
            right: "0%"
        }, 500, function () {
        });
        $(".c-menu-close").animate({
            opacity: "1"
        }, 500, function () {
        });
        $(".c-bg").animate({
            opacity: "1"
        }, 500, function () {
        });
        $('body').addClass('overflow-hidden open');
    });
    $(".c-menu-close").click(function () {
        $(".c-menu").animate({
            right: "-100%"
        }, 500, function () {
        });
        $(".c-menu-close").animate({
            opacity: "0"
        }, 500, function () {
        });
        $(".c-bg").animate({
            opacity: "0"
        }, 500, function () {
        });
        setTimeout(function () {
            $('body').removeClass('overflow-hidden open');
        }, 500);
    });
    setTimeout(function () {
        $(".c-preloader").animate({
            opacity: "0"
        }, 3000, function () {
            $(".c-preloader").addClass('d-none');
        });
    }, 1500);
    $('.c-preloader').click(function () {
        $(this).addClass('d-none');
    });
    $(".login").click(function (e) {
        e.preventDefault();
        $('.c-form-login').removeClass('d-none');
        $('.c-form-reg').addClass('d-none');
    });
    $(".reg").click(function (e) {
        e.preventDefault();
        $('.c-form-reg').removeClass('d-none');
        $('.c-form-login').addClass('d-none');
    });
    $(".c-form-close-auth").click(function (e) {
        e.preventDefault();
        $('.c-form-login, .c-form-reg').addClass('d-none');
    });
    $('.open-channel-tv').click(function () {
        $('.c-window-channel-list').removeClass('d-none');
    });
    $('.c-window-close').click(function (e) {
        e.preventDefault();
        $('.c-window').addClass('d-none');
    });
    $('.inlogin').click(function (e) {
        e.preventDefault();
        $('.inloginsystem').removeClass('d-none');
        $('.outlogin').addClass('d-none');
    });
    $('.c-menu-setting').click(function () {
        $('.c-window-profile').removeClass('d-none');
    }); // setting email
    $('.setting-email').click(function () {
        $('.c-window-user-email').removeClass('d-none');
    });
    $('.setting-pass').click(function () {
        $('.c-window-user-password').removeClass('d-none');
    });
    $('.setting-pincode').click(function () {
        $('.c-window-user-pincode').removeClass('d-none');
    });
    $('.setting-time').click(function () {
        $('.c-window-user-time').removeClass('d-none');
    });
    $('.c-form-label__code input').on('change', function () {
        this.click(function () {
            if (this.value == "") {
                this.removeClass('active');
            } else {
                this.addClass('active');
            }
        });
    });
    $('.c-block__price').click(function () {
        $('body').addClass('overflow-hidden body-dark');
        $(".c-block-tarif").removeClass('d-none');
        $(".c-block-tarif").animate({
            bottom: "0%"
        }, 1000, function () {
        });
    });
    $('.movie__rating__btn').click(function () {
        $('body').addClass('overflow-hidden body-dark');
        $(".c-block-rating").removeClass('d-none');
        $(".c-block-rating").animate({
            bottom: "0%"
        }, 1000, function () {
        });
    });
    $('.movie__btn-watch').click(function () {
        $('body').addClass('overflow-hidden body-dark');
        $(".c-block-quality").removeClass('d-none');
        $(".c-block-quality").animate({
            bottom: "0%"
        }, 1000, function () {
        });
    });
    $('.c-block-tarif .c-line__tarif').click(function () {
        $(".c-block-tarif").animate({
            bottom: "-100%"
        }, 1000, function () {
        });
        setTimeout(function () {
            $('body').removeClass('overflow-hidden body-dark');
            $(".c-block-tarif").addClass('d-none');
        }, 1000);
    });
    $('.c-block-quality .c-line__tarif').click(function () {
        $(".c-block-quality").animate({
            bottom: "-100%"
        }, 1000, function () {
        });
        setTimeout(function () {
            $('body').removeClass('overflow-hidden body-dark');
            $(".c-block-quality").addClass('d-none');
        }, 1000);
    });
    $('.c-block-rating .c-line__tarif').click(function () {
        $(".c-block-rating").animate({
            bottom: "-100%"
        }, 1000, function () {
        });
        setTimeout(function () {
            $('body').removeClass('overflow-hidden body-dark');
            $(".c-block-rating").addClass('d-none');
        }, 1000);
    });
    $(".c-tabs .c-tabs__item").click(function (e) {
        $(".c-tabs .c-tabs__item").removeClass('active')
        $(this).addClass('active')
    });
});
[].map.call(document.querySelectorAll('[anim="ripple"]'), function (el) {
    el.addEventListener('click', function (e) {
        e = e.touches ? e.touches[0] : e;
        var r = el.getBoundingClientRect(),
            d = Math.sqrt(Math.pow(r.width, 2) + Math.pow(r.height, 2)) * 2;
        el.style.cssText = "--s: 0; --o: 1;";
        el.offsetTop;
        el.style.cssText = "--t: 1; --o: 0; --d: ".concat(d, "; --x:").concat(e.clientX - r.left, "; --y:").concat(e.clientY - r.top, ";");
    });
});
window.addEventListener("orientationchange", function () {
    if (window.orientation == "90") {
        if ($('.c-window-screen').hasClass("d-none")) {
        } else {
            $('.videorotate').removeClass("d-none");
        }
    }
}, false);

var swiper = new Swiper('.swiper-container__tabs', {
    slidesPerView: 'auto',
    spaceBetween: 5,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swipertabs2 = new Swiper('.swiper-container__tabs2', {
    slidesPerView: 'auto',
    spaceBetween: 5,
    grabCursor: true,
    init: false,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_tv = new Swiper('.swiper-container__tv', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_recommended = new Swiper('.swiper-container__recommended', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_free = new Swiper('.swiper-container__free', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_new = new Swiper('.swiper-container__new', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_man = new Swiper('.swiper-container__man', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_romantic = new Swiper('.swiper-container__romantic', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_horror = new Swiper('.swiper-container__horror', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiper_tvs = new Swiper('.swiper-container__tvs', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    init: false,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiperDr = new Swiper('.swiper-container__dr', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    grabCursor: true,
    init: false,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
var swiperChannels = new Swiper('.swiper-container__channels', {
    slidesPerView: 'auto',
    spaceBetween: 5,
    grabCursor: true,
    // pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true
    // }
});
$(document).ready(function () {
    $('.c-open-channel').click(function (e) {
        e.preventDefault();
        $('.c-window-channel').removeClass('d-none');
        swiper_tvs.init();
        swipertabs2.init();
    });
    $('.open-tv').click(function (e) {
        e.preventDefault();
        $('.c-window-screen').removeClass('d-none');
        swiperDr.init();
    });
    $('.c-menu__link-cinema').click(function () {
        $('.c-window-cinema').removeClass('d-none');
        var swiperCinemaBanner = new Swiper('.swiper-container__cinema-banner', {
            slidesPerView: '1',
            spaceBetween: 15,
            freeMode: false,
            grabCursor: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            }
        });
    });
    $('.c-window-cinema .c-tvlist__item').click(function () {
        $('.c-window-movie').removeClass('d-none');
    });
    const controls = `<div class="plyr__controls">
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="play" aria-label="Play, View From A Blue Moon">
    <svg class="icon--pressed" aria-hidden="true" focusable="false">
      <use xlink:href="#plyr-pause">
      </use>
    </svg>
    <svg class="icon--not-pressed" aria-hidden="true" focusable="false" width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M16.4639 7.49994L7.88565 2.88327C7.42146 2.63174 6.89461 2.49997 6.3586 2.50135C5.82259 2.50274 5.29651 2.63722 4.83381 2.89114C4.37111 3.14506 3.98826 3.50938 3.72416 3.94709C3.46005 4.3848 3.3241 4.8803 3.3301 5.38327V14.6499C3.3301 15.4058 3.65007 16.1307 4.21963 16.6652C4.78918 17.1997 5.56166 17.4999 6.36713 17.4999C6.90033 17.4991 7.42395 17.3669 7.88565 17.1166L16.4639 12.4999C16.9249 12.2496 17.3075 11.8899 17.5736 11.457C17.8396 11.024 17.9797 10.533 17.9797 10.0333C17.9797 9.5335 17.8396 9.04252 17.5736 8.60958C17.3075 8.17663 16.9249 7.81695 16.4639 7.56661V7.49994Z" fill="white"/>
    </svg>
  </button>
  <div class="plyr__controls__item plyr__progress__container">
    <div class="plyr__progress">
      <input data-plyr="seek" type="range" min="0" max="100" step="0.01" value="0" autocomplete="off" role="slider" aria-label="Seek" aria-valuemin="0" aria-valuemax="183.126" aria-valuenow="0" id="plyr-seek-127" aria-valuetext="00:00 of 00:00" style="--value:0%;">
      <progress class="plyr__progress__buffer" min="0" max="100" value="3.8410711750379516" role="progressbar" aria-hidden="true">% buffered
      </progress>
      <span class="plyr__tooltip" hidden="">00:00
      </span>
      <div class="plyr__preview-thumb">
        <div class="plyr__preview-thumb__image-container">
        </div>
        <div class="plyr__preview-thumb__time-container">
          <span>00:00
          </span>
        </div>
      </div>
    </div>
  </div>
  <div class="plyr__controls__item plyr__time--current plyr__time" aria-label="Current time">
  03:03
  </div>
  <div class="plyr__controls__item plyr__controls__next" aria-label="Next">
    Далее: Новости (с субтитрами)
  </div>
  <div class="plyr__controls__item plyr__volume">
    <button type="button" class="plyr__control" data-plyr="mute">
      <svg class="icon--pressed" aria-hidden="true" focusable="false">
        <use xlink:href="#plyr-muted">
        </use>
      </svg>
      <svg class="icon--not-pressed" aria-hidden="true" focusable="false" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M10.3583 3.41665C10.223 3.3581 10.0746 3.33651 9.92815 3.35408C9.78175 3.37165 9.64263 3.42775 9.52499 3.51665L5.54166 6.66665H2.49999C2.27898 6.66665 2.06701 6.75445 1.91073 6.91073C1.75445 7.06701 1.66666 7.27897 1.66666 7.49999V12.5C1.66666 12.721 1.75445 12.933 1.91073 13.0892C2.06701 13.2455 2.27898 13.3333 2.49999 13.3333H5.54166L9.48332 16.4833C9.62994 16.601 9.81202 16.6656 9.99999 16.6667C10.1245 16.6687 10.2475 16.6401 10.3583 16.5833C10.5001 16.5158 10.62 16.4095 10.704 16.2768C10.7881 16.1441 10.8329 15.9904 10.8333 15.8333V4.16665C10.8329 4.00958 10.7881 3.85583 10.704 3.72313C10.62 3.59044 10.5001 3.48419 10.3583 3.41665ZM9.16666 14.1L6.34999 11.85C6.20337 11.7324 6.02129 11.6677 5.83332 11.6667H3.33332V8.33332H5.83332C6.02129 8.33223 6.20337 8.26762 6.34999 8.14999L9.16666 5.89999V14.1ZM16.3833 5.28332C16.2264 5.1264 16.0136 5.03824 15.7917 5.03824C15.5697 5.03824 15.3569 5.1264 15.2 5.28332C15.0431 5.44024 14.9549 5.65307 14.9549 5.87499C14.9549 6.0969 15.0431 6.30973 15.2 6.46665C15.6907 6.9566 16.0741 7.54326 16.3259 8.18936C16.5777 8.83546 16.6924 9.52687 16.6626 10.2197C16.6328 10.9125 16.4592 11.5915 16.1529 12.2136C15.8466 12.8357 15.4143 13.3873 14.8833 13.8333C14.7544 13.9436 14.6622 14.0906 14.619 14.2546C14.5758 14.4187 14.5837 14.592 14.6417 14.7515C14.6997 14.9109 14.805 15.0489 14.9434 15.1469C15.0819 15.2449 15.247 15.2983 15.4167 15.3C15.6114 15.3004 15.8001 15.2326 15.95 15.1083C16.6591 14.5144 17.2369 13.7794 17.6465 12.9501C18.0562 12.1208 18.2888 11.2153 18.3296 10.2912C18.3703 9.36713 18.2183 8.44468 17.8833 7.58252C17.5482 6.72036 17.0374 5.93736 16.3833 5.28332ZM14.025 7.64165C13.9473 7.56395 13.8551 7.50232 13.7535 7.46027C13.652 7.41822 13.5432 7.39658 13.4333 7.39658C13.3234 7.39658 13.2146 7.41822 13.1131 7.46027C13.0116 7.50232 12.9194 7.56395 12.8417 7.64165C12.764 7.71935 12.7023 7.81159 12.6603 7.91311C12.6182 8.01463 12.5966 8.12344 12.5966 8.23332C12.5966 8.3432 12.6182 8.45201 12.6603 8.55353C12.7023 8.65505 12.764 8.74729 12.8417 8.82499C13.1546 9.13611 13.3315 9.55867 13.3333 9.99999C13.3335 10.2428 13.2807 10.4827 13.1785 10.7029C13.0763 10.9231 12.9272 11.1184 12.7417 11.275C12.6573 11.3449 12.5875 11.4309 12.5364 11.5278C12.4853 11.6248 12.4538 11.7309 12.4437 11.84C12.4336 11.9492 12.4452 12.0592 12.4778 12.1639C12.5103 12.2686 12.5632 12.3658 12.6333 12.45C12.7039 12.5338 12.7902 12.6028 12.8875 12.6532C12.9847 12.7036 13.091 12.7343 13.2001 12.7436C13.3092 12.7528 13.4191 12.7405 13.5235 12.7073C13.6278 12.6741 13.7246 12.6206 13.8083 12.55C14.1809 12.2376 14.4807 11.8474 14.6865 11.4069C14.8924 10.9664 14.9994 10.4862 15 9.99999C14.9953 9.11674 14.6454 8.27036 14.025 7.64165Z" fill="white"/>
      </svg>
    </button>
    <input class="d-none" data-plyr="volume" type="range" min="0" max="1" step="0.05" value="1" autocomplete="off" role="slider" aria-label="Volume" aria-valuemin="0" aria-valuemax="100" aria-valuenow="10" id="plyr-volume-127" aria-valuetext="10.0%" style="--value:10%;">
  </div>
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="captions">
    <svg class="icon--pressed" aria-hidden="true" focusable="false">
      <use xlink:href="#plyr-captions-on">
      </use>
    </svg>
    <svg class="icon--not-pressed" aria-hidden="true" focusable="false">
      <use xlink:href="#plyr-captions-off">
      </use>
    </svg>
  </button>
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="pip">
    <svg aria-hidden="true" focusable="false" width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="30" height="30" rx="10" fill="#9D67F8"/>
      <path d="M11.6667 11.3333H12.1667V10.8333V7.49996C12.1667 7.41155 12.2018 7.32677 12.2643 7.26426C12.3268 7.20174 12.4116 7.16663 12.5 7.16663H22.5C22.5884 7.16663 22.6732 7.20175 22.7357 7.26426C22.7982 7.32677 22.8333 7.41155 22.8333 7.49996V17.5C22.8333 17.5884 22.7982 17.6731 22.7357 17.7357C22.6732 17.7982 22.5884 17.8333 22.5 17.8333H19.1667H18.6667V18.3333V20C18.6667 20.0884 18.6315 20.1732 18.569 20.2357C18.5065 20.2982 18.4217 20.3333 18.3333 20.3333H15H14.5V20.8333V22.5C14.5 22.5884 14.4649 22.6732 14.4024 22.7357C14.3398 22.7982 14.2551 22.8333 14.1667 22.8333H7.49999C7.41158 22.8333 7.3268 22.7982 7.26429 22.7357C7.20178 22.6732 7.16666 22.5884 7.16666 22.5V15.8333C7.16666 15.7449 7.20178 15.6601 7.26429 15.5976C7.3268 15.5351 7.41158 15.5 7.49999 15.5H9.16666H9.66666V15V11.6666C9.66666 11.5782 9.70178 11.4934 9.76429 11.4309C9.8268 11.3684 9.91158 11.3333 9.99999 11.3333H11.6667ZM13.3333 22.1666H13.8333V21.6666V16.6666V16.1666H13.3333H8.33332H7.83332V16.6666V21.6666V22.1666H8.33332H13.3333ZM17.5 19.6666H18V19.1666V12.5V12H17.5H10.8333H10.3333V12.5V15V15.5H10.8333H14.1667C14.2551 15.5 14.3398 15.5351 14.4024 15.5976C14.4649 15.6601 14.5 15.7449 14.5 15.8333V19.1666V19.6666H15H17.5ZM21.6667 17.1666H22.1667V16.6666V8.33329V7.83329H21.6667H13.3333H12.8333V8.33329V10.8333V11.3333H13.3333H18.3333C18.4217 11.3333 18.5065 11.3684 18.569 11.4309C18.6315 11.4934 18.6667 11.5782 18.6667 11.6666V16.6666V17.1666H19.1667H21.6667Z" fill="#93ABC8" stroke="white"/>
    </svg>
  </button>
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="share">
   <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
     <path d="M15 18.3334C16.3807 18.3334 17.5 17.2141 17.5 15.8334C17.5 14.4527 16.3807 13.3334 15 13.3334C13.6193 13.3334 12.5 14.4527 12.5 15.8334C12.5 17.2141 13.6193 18.3334 15 18.3334Z" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" fill="none"></path>
     <path d="M5 12.5C6.38071 12.5 7.5 11.3807 7.5 10C7.5 8.61929 6.38071 7.5 5 7.5C3.61929 7.5 2.5 8.61929 2.5 10C2.5 11.3807 3.61929 12.5 5 12.5Z" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" fill="none"></path>
     <path d="M7.15833 11.2583L12.85 14.575" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
     <path d="M15 6.66663C16.3807 6.66663 17.5 5.54734 17.5 4.16663C17.5 2.78591 16.3807 1.66663 15 1.66663C13.6193 1.66663 12.5 2.78591 12.5 4.16663C12.5 5.54734 13.6193 6.66663 15 6.66663Z" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" fill="none"></path>
     <path d="M12.8417 5.42505L7.15833 8.74171" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
   </svg>
  </button>
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="list">
   <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M3.09167 13.575C3.01241 13.4991 2.91896 13.4397 2.81667 13.4C2.61378 13.3167 2.38622 13.3167 2.18333 13.4C2.08104 13.4397 1.98759 13.4991 1.90833 13.575C1.83247 13.6543 1.773 13.7477 1.73333 13.85C1.66952 14.0018 1.65208 14.169 1.68323 14.3307C1.71437 14.4923 1.7927 14.6411 1.90833 14.7583C1.98933 14.8319 2.08235 14.8911 2.18333 14.9333C2.28308 14.9774 2.39094 15.0002 2.5 15.0002C2.60906 15.0002 2.71692 14.9774 2.81667 14.9333C2.91765 14.8911 3.01067 14.8319 3.09167 14.7583C3.2073 14.6411 3.28563 14.4923 3.31677 14.3307C3.34792 14.169 3.33048 14.0018 3.26667 13.85C3.22701 13.7477 3.16753 13.6543 3.09167 13.575ZM5.83333 6.66667H17.5C17.721 6.66667 17.933 6.57887 18.0893 6.42259C18.2455 6.26631 18.3333 6.05435 18.3333 5.83333C18.3333 5.61232 18.2455 5.40036 18.0893 5.24408C17.933 5.0878 17.721 5 17.5 5H5.83333C5.61232 5 5.40036 5.0878 5.24408 5.24408C5.0878 5.40036 5 5.61232 5 5.83333C5 6.05435 5.0878 6.26631 5.24408 6.42259C5.40036 6.57887 5.61232 6.66667 5.83333 6.66667ZM3.09167 9.40833C2.97448 9.2927 2.82567 9.21437 2.66401 9.18323C2.50235 9.15208 2.33509 9.16952 2.18333 9.23333C2.08235 9.27554 1.98933 9.33472 1.90833 9.40833C1.83247 9.48759 1.773 9.58104 1.73333 9.68333C1.68925 9.78308 1.66647 9.89094 1.66647 10C1.66647 10.1091 1.68925 10.2169 1.73333 10.3167C1.77554 10.4177 1.83472 10.5107 1.90833 10.5917C1.98933 10.6653 2.08235 10.7245 2.18333 10.7667C2.28308 10.8108 2.39094 10.8335 2.5 10.8335C2.60906 10.8335 2.71692 10.8108 2.81667 10.7667C2.91765 10.7245 3.01067 10.6653 3.09167 10.5917C3.16528 10.5107 3.22447 10.4177 3.26667 10.3167C3.31075 10.2169 3.33353 10.1091 3.33353 10C3.33353 9.89094 3.31075 9.78308 3.26667 9.68333C3.22701 9.58104 3.16753 9.48759 3.09167 9.40833ZM17.5 9.16667H5.83333C5.61232 9.16667 5.40036 9.25446 5.24408 9.41074C5.0878 9.56703 5 9.77899 5 10C5 10.221 5.0878 10.433 5.24408 10.5893C5.40036 10.7455 5.61232 10.8333 5.83333 10.8333H17.5C17.721 10.8333 17.933 10.7455 18.0893 10.5893C18.2455 10.433 18.3333 10.221 18.3333 10C18.3333 9.77899 18.2455 9.56703 18.0893 9.41074C17.933 9.25446 17.721 9.16667 17.5 9.16667ZM3.09167 5.24167C3.01241 5.1658 2.91896 5.10633 2.81667 5.06667C2.66491 5.00285 2.49765 4.98542 2.33599 5.01656C2.17433 5.04771 2.02552 5.12604 1.90833 5.24167C1.83472 5.32267 1.77554 5.41568 1.73333 5.51667C1.68925 5.61642 1.66647 5.72427 1.66647 5.83333C1.66647 5.94239 1.68925 6.05025 1.73333 6.15C1.77554 6.25099 1.83472 6.344 1.90833 6.425C1.98933 6.49861 2.08235 6.5578 2.18333 6.6C2.33509 6.66382 2.50235 6.68125 2.66401 6.65011C2.82567 6.61896 2.97448 6.54063 3.09167 6.425C3.16528 6.344 3.22447 6.25099 3.26667 6.15C3.31075 6.05025 3.33353 5.94239 3.33353 5.83333C3.33353 5.72427 3.31075 5.61642 3.26667 5.51667C3.22447 5.41568 3.16528 5.32267 3.09167 5.24167ZM17.5 13.3333H5.83333C5.61232 13.3333 5.40036 13.4211 5.24408 13.5774C5.0878 13.7337 5 13.9457 5 14.1667C5 14.3877 5.0878 14.5996 5.24408 14.7559C5.40036 14.9122 5.61232 15 5.83333 15H17.5C17.721 15 17.933 14.9122 18.0893 14.7559C18.2455 14.5996 18.3333 14.3877 18.3333 14.1667C18.3333 13.9457 18.2455 13.7337 18.0893 13.5774C17.933 13.4211 17.721 13.3333 17.5 13.3333Z" fill="white"/>
   </svg>
  </button>
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="back">
   <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M12.5 15L7.5 10L12.5 5" stroke="#93ABC8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" fill="none"/>
   </svg>
  </button>
  <div class="plyr__controls__item plyr__controls__title">
    <h4>Не лечи меня</h4>
    <div class="plyr__controls__title__channel">
      <img src="images/channel-icon.png" alt="Icon">
      <span>Вместе РФ</span>
    </div>
  </div>
  <button class="plyr__controls__item plyr__control" type="button" data-plyr="fullscreen">
    <svg class="icon--pressed" aria-hidden="true" focusable="false" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.3333 7.49996H17.5C17.721 7.49996 17.933 7.41216 18.0892 7.25588C18.2455 7.0996 18.3333 6.88764 18.3333 6.66663C18.3333 6.44561 18.2455 6.23365 18.0892 6.07737C17.933 5.92109 17.721 5.83329 17.5 5.83329H14.1667V2.49996C14.1667 2.27895 14.0789 2.06698 13.9226 1.9107C13.7663 1.75442 13.5543 1.66663 13.3333 1.66663C13.1123 1.66663 12.9003 1.75442 12.7441 1.9107C12.5878 2.06698 12.5 2.27895 12.5 2.49996V6.66663C12.5 6.88764 12.5878 7.0996 12.7441 7.25588C12.9003 7.41216 13.1123 7.49996 13.3333 7.49996ZM6.66666 12.5H2.49999C2.27898 12.5 2.06701 12.5878 1.91073 12.744C1.75445 12.9003 1.66666 13.1123 1.66666 13.3333C1.66666 13.5543 1.75445 13.7663 1.91073 13.9225C2.06701 14.0788 2.27898 14.1666 2.49999 14.1666H5.83332V17.5C5.83332 17.721 5.92112 17.9329 6.0774 18.0892C6.23368 18.2455 6.44564 18.3333 6.66666 18.3333C6.88767 18.3333 7.09963 18.2455 7.25591 18.0892C7.41219 17.9329 7.49999 17.721 7.49999 17.5V13.3333C7.49999 13.1123 7.41219 12.9003 7.25591 12.744C7.09963 12.5878 6.88767 12.5 6.66666 12.5ZM6.66666 1.66663C6.44564 1.66663 6.23368 1.75442 6.0774 1.9107C5.92112 2.06698 5.83332 2.27895 5.83332 2.49996V5.83329H2.49999C2.27898 5.83329 2.06701 5.92109 1.91073 6.07737C1.75445 6.23365 1.66666 6.44561 1.66666 6.66663C1.66666 6.88764 1.75445 7.0996 1.91073 7.25588C2.06701 7.41216 2.27898 7.49996 2.49999 7.49996H6.66666C6.88767 7.49996 7.09963 7.41216 7.25591 7.25588C7.41219 7.0996 7.49999 6.88764 7.49999 6.66663V2.49996C7.49999 2.27895 7.41219 2.06698 7.25591 1.9107C7.09963 1.75442 6.88767 1.66663 6.66666 1.66663ZM17.5 12.5H13.3333C13.1123 12.5 12.9003 12.5878 12.7441 12.744C12.5878 12.9003 12.5 13.1123 12.5 13.3333V17.5C12.5 17.721 12.5878 17.9329 12.7441 18.0892C12.9003 18.2455 13.1123 18.3333 13.3333 18.3333C13.5543 18.3333 13.7663 18.2455 13.9226 18.0892C14.0789 17.9329 14.1667 17.721 14.1667 17.5V14.1666H17.5C17.721 14.1666 17.933 14.0788 18.0892 13.9225C18.2455 13.7663 18.3333 13.5543 18.3333 13.3333C18.3333 13.1123 18.2455 12.9003 18.0892 12.744C17.933 12.5878 17.721 12.5 17.5 12.5Z" fill="white"/>
    </svg>
    <svg class="icon--not-pressed" aria-hidden="true" focusable="false" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M6.66663 1.66663H2.49996C2.27895 1.66663 2.06698 1.75442 1.9107 1.9107C1.75442 2.06698 1.66663 2.27895 1.66663 2.49996V6.66663C1.66663 6.88764 1.75442 7.0996 1.9107 7.25588C2.06698 7.41216 2.27895 7.49996 2.49996 7.49996C2.72097 7.49996 2.93293 7.41216 3.08922 7.25588C3.2455 7.0996 3.33329 6.88764 3.33329 6.66663V3.33329H6.66663C6.88764 3.33329 7.0996 3.2455 7.25588 3.08922C7.41216 2.93293 7.49996 2.72097 7.49996 2.49996C7.49996 2.27895 7.41216 2.06698 7.25588 1.9107C7.0996 1.75442 6.88764 1.66663 6.66663 1.66663ZM6.66663 16.6666H3.33329V13.3333C3.33329 13.1123 3.2455 12.9003 3.08922 12.744C2.93293 12.5878 2.72097 12.5 2.49996 12.5C2.27895 12.5 2.06698 12.5878 1.9107 12.744C1.75442 12.9003 1.66663 13.1123 1.66663 13.3333V17.5C1.66663 17.721 1.75442 17.9329 1.9107 18.0892C2.06698 18.2455 2.27895 18.3333 2.49996 18.3333H6.66663C6.88764 18.3333 7.0996 18.2455 7.25588 18.0892C7.41216 17.9329 7.49996 17.721 7.49996 17.5C7.49996 17.2789 7.41216 17.067 7.25588 16.9107C7.0996 16.7544 6.88764 16.6666 6.66663 16.6666ZM17.5 1.66663H13.3333C13.1123 1.66663 12.9003 1.75442 12.744 1.9107C12.5878 2.06698 12.5 2.27895 12.5 2.49996C12.5 2.72097 12.5878 2.93293 12.744 3.08922C12.9003 3.2455 13.1123 3.33329 13.3333 3.33329H16.6666V6.66663C16.6666 6.88764 16.7544 7.0996 16.9107 7.25588C17.067 7.41216 17.2789 7.49996 17.5 7.49996C17.721 7.49996 17.9329 7.41216 18.0892 7.25588C18.2455 7.0996 18.3333 6.88764 18.3333 6.66663V2.49996C18.3333 2.27895 18.2455 2.06698 18.0892 1.9107C17.9329 1.75442 17.721 1.66663 17.5 1.66663ZM17.5 12.5C17.2789 12.5 17.067 12.5878 16.9107 12.744C16.7544 12.9003 16.6666 13.1123 16.6666 13.3333V16.6666H13.3333C13.1123 16.6666 12.9003 16.7544 12.744 16.9107C12.5878 17.067 12.5 17.2789 12.5 17.5C12.5 17.721 12.5878 17.9329 12.744 18.0892C12.9003 18.2455 13.1123 18.3333 13.3333 18.3333H17.5C17.721 18.3333 17.9329 18.2455 18.0892 18.0892C18.2455 17.9329 18.3333 17.721 18.3333 17.5V13.3333C18.3333 13.1123 18.2455 12.9003 18.0892 12.744C17.9329 12.5878 17.721 12.5 17.5 12.5Z" fill="white"/>
    </svg>
  </button>
  <div class="plyr__right d-none">
  <div class="plyr__right__line"></div>
    <ul class="plyr__right__list">
        <li class="plyr__list-item">
          <span class="plyr__list-item__title">Touch TV</span>
          <span class="plyr__list-item__text">Высокого полёта Touc</span>
        </li>
         <li class="plyr__list-item">
          <span class="plyr__list-item__title">Okko TV</span>
          <span class="plyr__list-item__text">Высокого полёта Okk</span>
        </li>
        <li class="plyr__list-item">
          <span class="plyr__list-item__title">Kinopoisk HD</span>
          <span class="plyr__list-item__text">Высокого полёта Kino</span>
        </li>
        <li class="plyr__list-item">
          <span class="plyr__list-item__title">IVI HD</span>
          <span class="plyr__list-item__text">Высокого полёта IVI</span>
        </li>
        <li class="plyr__list-item">
          <span class="plyr__list-item__title">Okko TV</span>
          <span class="plyr__list-item__text">Высокого полёта Okk</span>
        </li>
    </ul>
  </div>
</div>
<button type="button" class="plyr__control plyr__control--overlaid" data-plyr="play" aria-label="Play, View From A Blue Moon">
    <svg aria-hidden="true" focusable="false" width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M37.0801 17.9999L17.7601 6.91993C16.7146 6.31625 15.5281 6.00001 14.3209 6.00332C13.1137 6.00664 11.9288 6.32939 10.8867 6.93881C9.84463 7.54822 8.98239 8.42259 8.38757 9.47309C7.79275 10.5236 7.48655 11.7128 7.50008 12.9199V35.1599C7.50008 36.974 8.22072 38.7138 9.50347 39.9965C10.7862 41.2793 12.526 41.9999 14.3401 41.9999C15.541 41.9979 16.7203 41.6807 17.7601 41.0799L37.0801 29.9999C38.1182 29.3991 38.9801 28.5359 39.5792 27.4968C40.1784 26.4577 40.4938 25.2794 40.4938 24.0799C40.4938 22.8805 40.1784 21.7021 39.5792 20.6631C38.9801 19.624 38.1182 18.7608 37.0801 18.1599V17.9999Z" fill="white"/>
    </svg>
</button>
`;
    const player = new Plyr('#player', {
        controls,
        invertTime: false
    });
    $('.plyr__controls__item[data-plyr="list"]').click(function () {
        $('body').addClass('overflow-hidden');
        $(".plyr__right").removeClass('d-none');
        $(".plyr__right").animate({
            right: "20px"
        }, 1000, function () {
        });
    });
    $('.plyr__right__line').click(function () {
        $(".plyr__right").animate({
            right: "-100%"
        }, 1000, function () {
        });
        setTimeout(function () {
            $('body').removeClass('overflow-hidden');
            $(".plyr__right").addClass('d-none');
        }, 1000);
    });
});
